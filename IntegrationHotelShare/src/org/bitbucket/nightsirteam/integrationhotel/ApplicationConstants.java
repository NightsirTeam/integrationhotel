package org.bitbucket.nightsirteam.integrationhotel;

public class ApplicationConstants {
	public static final String RMI_ADDRESS = "localhost";
	public static final String RMI_NAME = "IntegrationDBAccess";
	public static final Integer RMI_PORT = 1099;

	public static final String DB_ADDRESS = "localhost";
	public static final Integer DB_PORT = 3306;

	public static final String DB_USER = "root";
	public static final String DB_PASSWORD = "";

	public static final String DB_NAME = "hotel";

	public static final String DB_TABLE_BENUTZER = "benutzer";
	public static final String DB_TABLE_BUCHUNG = "buchung";
	public static final String DB_TABLE_LEISTUNG = "leistung";
	public static final String DB_TABLE_PERSON = "person";
	public static final String DB_TABLE_POSITION = "position";
}