package org.bitbucket.nightsirteam.integrationhotel;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.Date;
import java.util.List;

import org.bitbucket.nightsirteam.integrationhotel.model.Buchung;
import org.bitbucket.nightsirteam.integrationhotel.model.Person;
import org.bitbucket.nightsirteam.integrationhotel.model.PersonenBuchung;

public interface IDataAccess extends Remote {
	public List<Person> filterPersonsByName(String vorname, String nachname) throws RemoteException;

	public List<Buchung> getBuchungenForPerson(int id) throws RemoteException;

	public List<PersonenBuchung> getPersonenBuchungForDate(long date) throws RemoteException;
}
