package org.bitbucket.nightsirteam.integrationhotel.model;

import java.io.Serializable;
import java.sql.Date;

public class Person implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String vorname;
	private String name;
	private String strasse;
	private String ort;
	private String privattelefon;
	private Date eingabedatum;

	public Person(int id, String vorname, String name, String strasse,
			String ort, String privattelefon, Date eingabedatum) {
		super();
		this.setId(id);
		this.vorname = vorname;
		this.name = name;
		this.strasse = strasse;
		this.ort = ort;
		this.privattelefon = privattelefon;
		this.eingabedatum = eingabedatum;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public String getPrivattelefon() {
		return privattelefon;
	}

	public void setPrivattelefon(String privattelefon) {
		this.privattelefon = privattelefon;
	}

	public Date getEingabedatum() {
		return eingabedatum;
	}

	public void setEingabedatum(Date eingabedatum) {
		this.eingabedatum = eingabedatum;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "(" + id + ") vorname: " + vorname + ", name: " + name
				+ ", ort: " + ort + ", strasse: " + strasse
				+ ", privattelefon: " + privattelefon + ", eingabedatum: "
				+ eingabedatum;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
