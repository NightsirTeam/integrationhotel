package org.bitbucket.nightsirteam.integrationhotel.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

public class Position implements Serializable {

	private static final long serialVersionUID = 1L;

	private int anzahl;
	private BigDecimal preis;
	private BigDecimal rabatt;
	private Date datum;
	private String beschreibung;

	public Position(int anzahl, BigDecimal preis, BigDecimal rabatt,
			Date datum, String beschreibung) {
		super();
		this.anzahl = anzahl;
		this.preis = preis;
		this.rabatt = rabatt;
		this.datum = datum;
		this.beschreibung = beschreibung;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	public BigDecimal getPreis() {
		return preis;
	}

	public void setPreis(BigDecimal preis) {
		this.preis = preis;
	}

	public BigDecimal getRabatt() {
		return rabatt;
	}

	public void setRabatt(BigDecimal rabatt) {
		this.rabatt = rabatt;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	@Override
	public String toString() {
		return "Position [anzahl=" + anzahl + ", preis=" + preis + ", rabatt="
				+ rabatt + ", datum=" + datum + ", beschreibung="
				+ beschreibung + "]";
	}

}
