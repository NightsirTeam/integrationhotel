package org.bitbucket.nightsirteam.integrationhotel.model;

import java.io.Serializable;
import java.sql.Date;

public class PersonenBuchung implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Person person;
	private Date ankunft;
	private Date abreise;
	
	
	public PersonenBuchung(Person person, Date ankunft, Date abreise) {
		super();
		this.person = person;
		this.ankunft = ankunft;
		this.abreise = abreise;
	}
	
	
	public Person getPerson() {
		return person;
	}
	public Date getAnkunft() {
		return ankunft;
	}
	public Date getAbreise() {
		return abreise;
	}
	
	
}



