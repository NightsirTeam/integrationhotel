package org.bitbucket.nightsirteam.integrationhotel.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public class Buchung implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private int personIdfs;
	private Date ankunft;
	private Date abreise;
	private List<Position> positionen;

	public Buchung(int id, int personIdfs, Date ankunft, Date abreise,
			List<Position> positionen) {
		super();
		this.id = id;
		this.personIdfs = personIdfs;
		this.ankunft = ankunft;
		this.abreise = abreise;
		this.positionen = positionen;
	}

	public int getId() {
		return id;
	}

	public int getPersonIdfs() {
		return personIdfs;
	}

	public Date getAnkunft() {
		return ankunft;
	}

	public Date getAbreise() {
		return abreise;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Position> getPositionen() {
		return positionen;
	}

	@Override
	public String toString() {
		return "Buchung [id=" + id + ", personIdfs=" + personIdfs
				+ ", ankunft=" + ankunft + ", abreise=" + abreise
				+ ", positionen=" + positionen + "]";
	}

}
