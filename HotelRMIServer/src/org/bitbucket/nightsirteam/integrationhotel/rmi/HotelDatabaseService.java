package org.bitbucket.nightsirteam.integrationhotel.rmi;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.bitbucket.nightsirteam.integrationhotel.ApplicationConstants;
import org.bitbucket.nightsirteam.integrationhotel.model.Buchung;
import org.bitbucket.nightsirteam.integrationhotel.model.Person;
import org.bitbucket.nightsirteam.integrationhotel.model.PersonenBuchung;
import org.bitbucket.nightsirteam.integrationhotel.model.Position;

public class HotelDatabaseService {

	private Connection conn;

	public HotelDatabaseService() {
		createConnection();
	}

	private void createConnection() {
		System.out.println("[HotelDatabaseService] Creating connection...");
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://"
					+ ApplicationConstants.DB_ADDRESS + ":"
					+ ApplicationConstants.DB_PORT + "/"
					+ ApplicationConstants.DB_NAME,
					ApplicationConstants.DB_USER,
					ApplicationConstants.DB_PASSWORD);
		} catch (ClassNotFoundException e) {
			throw new RmiServerException("Can't find mysql driver class", e);
		} catch (SQLException e) {
			throw new RmiServerException("Can't connect to db", e);
		}
	}

	private void refreshConnection() {
		try {
			if (conn.isValid(1)) {
				return;
			}

			conn.close();
		} catch (SQLException e) {
			throw new RmiServerException("Can't refresh connection", e);
		}

		System.out
				.println("[HotelDatabaseService] Connection was invalid. Trying to recreate Connection.");
		createConnection();

	}

	private static Person getPersonFromEntity(ResultSet resultSet) {
		String vorname = null;
		String name = null;
		String strasse = null;
		String ort = null;
		String telefon = null;
		Date datum = null;
		int id;

		try {
			vorname = resultSet.getString("Vorname");

			name = resultSet.getString("Name");
			strasse = resultSet.getString("Strasse");
			ort = resultSet.getString("Ort");
			telefon = resultSet.getString("Privattelefon");
			datum = resultSet.getDate("Eingabedatum");
			id = resultSet.getInt("Person_ID");

		} catch (SQLException e) {
			throw new RmiServerException("can't get person from ResultSet", e);
		}

		return new Person(id, vorname, name, strasse, ort, telefon, datum);

	}

	public List<Person> filterPersonsByName(String vorname, String nachname)
			throws SQLException {

		List<Person> personList = new ArrayList<Person>();

		refreshConnection();

		PreparedStatement stmt = conn.prepareStatement("Select * from "
				+ ApplicationConstants.DB_TABLE_PERSON
				+ " where Vorname like ? and Name like ?");

		stmt.setString(1, "%" + vorname + "%");
		stmt.setString(2, "%" + nachname + "%");

		ResultSet entries = stmt.executeQuery();

		while (entries.next()) {

			personList.add(getPersonFromEntity(entries));
		}

		entries.close();

		stmt.close();

		System.out
				.println("[HotelDatabaseService] filterPersonsByName returning "
						+ personList.size() + " items.");
		return personList;
	}

	private List<Position> getAllPositionenForBuchung(int id)
			throws SQLException {

		PreparedStatement innerStmt = conn.prepareStatement("Select * from "
				+ ApplicationConstants.DB_TABLE_POSITION + ", "
				+ ApplicationConstants.DB_TABLE_LEISTUNG
				+ " where Leistung_IDFS = Leistung_ID  and Buchung_IDFS = ?");

		innerStmt.setInt(1, id);
		ResultSet innerEntries = innerStmt.executeQuery();

		List<Position> positionen = new ArrayList<Position>();
		while (innerEntries.next()) {

			int anzahl = innerEntries.getInt("Anzahl");
			BigDecimal preis = innerEntries.getBigDecimal("Preis");
			BigDecimal rabatt = innerEntries.getBigDecimal("Rabatt");
			Date datum = innerEntries.getDate("Datum");
			String beschreibung = innerEntries.getString("Beschreibung");

			positionen.add(new Position(anzahl, preis, rabatt, datum,
					beschreibung));
		}

		return positionen;
	}

	public List<Buchung> getBuchungenForPerson(int id) throws SQLException {
		List<Buchung> buchungList = new ArrayList<Buchung>();

		refreshConnection();

		PreparedStatement stmt = conn.prepareStatement("Select * from "
				+ ApplicationConstants.DB_TABLE_BUCHUNG
				+ " where Person_IDFS = ?");

		stmt.setInt(1, id);

		ResultSet entries = stmt.executeQuery();

		while (entries.next()) {

			int buchungId = entries.getInt("Buchung_ID");
			int personIdfs = entries.getInt("Person_IDFS");
			Date ankunft = entries.getDate("Ankunft");
			Date abreise = entries.getDate("Abreise");
			List<Position> positionen = getAllPositionenForBuchung(buchungId);

			Buchung newBuchung = new Buchung(buchungId, personIdfs, ankunft,
					abreise, positionen);

			buchungList.add(newBuchung);

		}

		entries.close();

		stmt.close();

		System.out
				.println("[HotelDatabaseService] getBuchungenForPerson returning "
						+ buchungList.size() + " items.");
		return buchungList;
	}

	/**
	 * 
	 * @param date
	 * @return
	 * @throws SQLException
	 */

	public List<PersonenBuchung> getPersonenBuchungForDate(Date date) throws SQLException {
		List<PersonenBuchung> personList = new ArrayList<PersonenBuchung>();

		refreshConnection();

		PreparedStatement stmt = conn
				.prepareStatement("Select * from "
						+ ApplicationConstants.DB_TABLE_PERSON
						+ ","
						+ ApplicationConstants.DB_TABLE_BUCHUNG
						+ " where Person_ID = Person_IDFS and ? < Abreise and ? > Ankunft");

		stmt.setDate(1, date);
		stmt.setDate(2, date);

		ResultSet entries = stmt.executeQuery();

		System.out.println("[DEBUG] stmt is " + stmt);
		while (entries.next()) {

			Date abreise = entries.getDate("Abreise");
			Date ankunft = entries.getDate("Ankunft");
			
			Person person = getPersonFromEntity(entries);
			
			personList.add(new PersonenBuchung(person, ankunft, abreise));
		}

		entries.close();

		stmt.close();

		System.out.println("[HotelDatabaseService] getPersonForDate returning "
				+ personList.size() + " items.");
		return personList;
	}
}