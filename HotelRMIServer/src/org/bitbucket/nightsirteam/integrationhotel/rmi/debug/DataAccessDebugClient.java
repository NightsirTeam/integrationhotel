package org.bitbucket.nightsirteam.integrationhotel.rmi.debug;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.List;

import org.bitbucket.nightsirteam.integrationhotel.ApplicationConstants;
import org.bitbucket.nightsirteam.integrationhotel.IDataAccess;
import org.bitbucket.nightsirteam.integrationhotel.model.Buchung;

public class DataAccessDebugClient {
	public static void main(String[] args) throws MalformedURLException,
			RemoteException, NotBoundException, ParseException {
		System.out.println("Start");
		IDataAccess fb = (IDataAccess) Naming.lookup("rmi://localhost:"
				+ ApplicationConstants.RMI_PORT + "/"
				+ ApplicationConstants.RMI_NAME);
		/*
		 * SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		 * java.util.Date parsed = format.parse("2002-11-10"); java.sql.Date sql
		 * = new java.sql.Date(parsed.getTime());
		 * 
		 * System.out.println(sql);
		 * 
		 * List<Person> persons = fb.getPersonForDate(sql);
		 * 
		 * System.out.println("persons" + persons); for (Person p : persons) {
		 * System.out.println(p); }
		 */

		List<Buchung> d = fb.getBuchungenForPerson(50);

		for (Buchung person : d) {
			System.out.println(person);
		}

		System.out.println("end");
	}
}
