package org.bitbucket.nightsirteam.integrationhotel.rmi;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import org.bitbucket.nightsirteam.integrationhotel.ApplicationConstants;

public class Server {
	public static void main(String[] args) {

		// Registry aus dem Programm starten
		System.out.println("[Server] Starting Server ...");
		try {
			System.out.println("[Server] Creating registry ...");
			LocateRegistry.createRegistry(ApplicationConstants.RMI_PORT);
		} catch (RemoteException e) {
			throw new RuntimeException("[Server] Registry already started", e);
		}

		// Service registrieren
		try {
			System.out.println("[Server] Binding impl ...");
			DataAccessImpl impl = new DataAccessImpl();
			System.out.println("[Server] Created instance!");

			System.out.println("[Server] Binding Server server...");
			Naming.rebind("//" + ApplicationConstants.RMI_ADDRESS + ":"
					+ ApplicationConstants.RMI_PORT + "/"
					+ ApplicationConstants.RMI_NAME, impl);

			System.out.println("[Server] Bound server!");
			System.out.println("[Server] All done...");
			System.out.println("[Server] ----------------");
			System.out.println("[Server] requests:");

		} catch (Exception exc) {
			System.out.println("[Server] Thrown error while rebinding");
			throw new RuntimeException(
					"Thrown error while rebinding RMI server", exc);
		}

	}

}
