package org.bitbucket.nightsirteam.integrationhotel.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import org.bitbucket.nightsirteam.integrationhotel.IDataAccess;
import org.bitbucket.nightsirteam.integrationhotel.model.Buchung;
import org.bitbucket.nightsirteam.integrationhotel.model.Person;
import org.bitbucket.nightsirteam.integrationhotel.model.PersonenBuchung;

public class DataAccessImpl extends UnicastRemoteObject implements IDataAccess {

	private static final long serialVersionUID = 1L;
	private HotelDatabaseService service;

	protected DataAccessImpl() throws RemoteException {
		super();

		service = new HotelDatabaseService();

	}

	@Override
	public List<Person> filterPersonsByName(String vorname, String nachname)
			throws RemoteException {
		System.out
				.println("[DataAccessImpl] filterPersonsByName called with parameter: \""
						+ vorname + "\" and \"" + nachname + "\"");

		try {
			return service.filterPersonsByName(vorname, nachname);
		} catch (SQLException e) {
			throw new RmiServerException(
					"Sql Exception thrown while trying to get PersonsByName in the HotelDatabaseService",
					e);
		}

	}

	@Override
	public List<Buchung> getBuchungenForPerson(int id) throws RemoteException {
		System.out
				.println("[DataAccessImpl] getBuchungenForPerson called with parameter: "
						+ id);

		try {
			return service.getBuchungenForPerson(id);
		} catch (SQLException e) {
			throw new RmiServerException(
					"Sql Exception thrown while trying to get BuchungenForPerson in the HotelDatabaseService",
					e);
		}

	}

	@Override
	public List<PersonenBuchung> getPersonenBuchungForDate(long date)
			throws RemoteException {
		System.out
				.println("[DataAccessImpl] getPersonForDate called with parameter: "
						+ date);
		try {
			return service.getPersonenBuchungForDate(new Date(date));
		} catch (SQLException e) {
			throw new RmiServerException(
					"Sql Exception thrown while trying to get PersonForDate in the HotelDatabaseService",
					e);
		}
	}

}
