package org.bitbucket.nightsirteam.integrationhotel.rmi;

public class RmiServerException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RmiServerException(String message, Throwable cause) {
		super(message, cause);
	}

}
