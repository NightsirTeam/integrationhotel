SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `hotel` DEFAULT CHARACTER SET latin1 COLLATE latin1_german1_ci ;
USE `hotel`;

-- -----------------------------------------------------
-- Table `hotel`.`person`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`person` ;

CREATE  TABLE IF NOT EXISTS `hotel`.`person` (
  `Person_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Vorname` VARCHAR(30) NOT NULL DEFAULT '' ,
  `Name` VARCHAR(30) NOT NULL DEFAULT '' ,
  `Strasse` VARCHAR(30) NULL DEFAULT NULL ,
  `Ort` VARCHAR(30) NULL DEFAULT NULL ,
  `Privattelefon` VARCHAR(20) NULL DEFAULT NULL ,
  `Eingabedatum` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`Person_ID`) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `hotel`.`buchung`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`buchung` ;

CREATE  TABLE IF NOT EXISTS `hotel`.`buchung` (
  `Buchung_ID` INT(11) NOT NULL DEFAULT '0' ,
  `Person_IDFS` INT(11) NULL DEFAULT NULL ,
  `Ankunft` DATETIME NULL DEFAULT NULL ,
  `Abreise` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`Buchung_ID`) ,
  INDEX `Person_IDFS` (`Person_IDFS` ASC) ,
  CONSTRAINT `Person_IDFS`
    FOREIGN KEY (`Person_IDFS` )
    REFERENCES `hotel`.`person` (`Person_ID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `hotel`.`leistung`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`leistung` ;

CREATE  TABLE IF NOT EXISTS `hotel`.`leistung` (
  `Leistung_ID` INT(11) NOT NULL DEFAULT '0' ,
  `Beschreibung` VARCHAR(70) NULL DEFAULT NULL ,
  PRIMARY KEY (`Leistung_ID`) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `hotel`.`benutzer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`benutzer` ;

CREATE  TABLE IF NOT EXISTS `hotel`.`benutzer` (
  `Benutzer_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Benutzer` VARCHAR(20) NOT NULL DEFAULT '' ,
  `Passwort` VARCHAR(20) NOT NULL DEFAULT '' ,
  `GanzerName` VARCHAR(50) NOT NULL DEFAULT '' ,
  PRIMARY KEY (`Benutzer_ID`) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `hotel`.`position`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`position` ;

CREATE  TABLE IF NOT EXISTS `hotel`.`position` (
  `Position_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Buchung_IDFS` INT(11) NULL DEFAULT NULL ,
  `Leistung_IDFS` INT(11) NULL DEFAULT NULL ,
  `Benutzer_IDFS` INT(11) NULL DEFAULT NULL ,
  `Anzahl` INT(11) NULL DEFAULT NULL ,
  `Preis` DECIMAL(9,2) NULL DEFAULT NULL ,
  `Rabatt` DECIMAL(4,2) NULL DEFAULT NULL ,
  `Datum` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`Position_ID`) ,
  INDEX `Buchung_IDFS` (`Buchung_IDFS` ASC) ,
  INDEX `Leistung_IDFS` (`Leistung_IDFS` ASC) ,
  INDEX `Benutzer_IDFS` (`Benutzer_IDFS` ASC) ,
  CONSTRAINT `Buchung_IDFS`
    FOREIGN KEY (`Buchung_IDFS` )
    REFERENCES `hotel`.`buchung` (`Buchung_ID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Leistung_IDFS`
    FOREIGN KEY (`Leistung_IDFS` )
    REFERENCES `hotel`.`leistung` (`Leistung_ID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Benutzer_IDFS`
    FOREIGN KEY (`Benutzer_IDFS` )
    REFERENCES `hotel`.`benutzer` (`Benutzer_ID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = MyISAM;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
