package org.bitbucket.nightsirteam.integrationhotel.rmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import org.bitbucket.nightsirteam.integrationhotel.ApplicationConstants;
import org.bitbucket.nightsirteam.integrationhotel.IDataAccess;
import org.bitbucket.nightsirteam.integrationhotel.exception.BuchungGettingException;
import org.bitbucket.nightsirteam.integrationhotel.exception.PersonGettingException;
import org.bitbucket.nightsirteam.integrationhotel.exception.ServerNotInitializableException;
import org.bitbucket.nightsirteam.integrationhotel.model.Buchung;
import org.bitbucket.nightsirteam.integrationhotel.model.NameFilter;
import org.bitbucket.nightsirteam.integrationhotel.model.Person;
import org.bitbucket.nightsirteam.integrationhotel.model.PersonenBuchung;

public class DBAccessor {
	private static DBAccessor instance;
	
	private IDataAccess da;
	
	private DBAccessor() throws ServerNotInitializableException {
		try {
			da = (IDataAccess) Naming.lookup("rmi://" + 
					ApplicationConstants.RMI_ADDRESS + ":" + ApplicationConstants.RMI_PORT + "/" +
					ApplicationConstants.RMI_NAME);
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			throw new ServerNotInitializableException(e);
		}
	}
	
	public static DBAccessor getInstance() throws ServerNotInitializableException {
		if(instance == null) {
			try {
				instance = new DBAccessor();
			} catch (ServerNotInitializableException e) {
				instance = null;
				throw e;
			}
		}
		return instance;
	}
	
	public List<Person> getPersonsFiltered(NameFilter filter) throws PersonGettingException {
		try {
			return da.filterPersonsByName(filter.getSurname(), filter.getLastname());
		} catch (Exception e) {
			throw new PersonGettingException(e);
		}
	}
	
	public List<Buchung> getBuchungByPersId(int id) {
		try {
			return da.getBuchungenForPerson(id);
		} catch (Exception e) {
			throw new BuchungGettingException(e);
		}
	}
	
	public List<PersonenBuchung> getPersonByDate(Date date) {
		try {
			return da.getPersonenBuchungForDate(date.getTime());
		} catch (Exception e) {
			throw new BuchungGettingException(e);
		}
	}
}
