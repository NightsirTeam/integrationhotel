package org.bitbucket.nightsirteam.integrationhotel.model;

public class NameFilter {
	private String surname;
	private String lastname;
	
	public NameFilter() {
		surname = "";
		lastname = "";
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
}
