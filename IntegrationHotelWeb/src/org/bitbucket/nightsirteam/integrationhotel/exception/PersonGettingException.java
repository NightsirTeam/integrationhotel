package org.bitbucket.nightsirteam.integrationhotel.exception;

public class PersonGettingException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PersonGettingException() {
		super();
	}
	
	public PersonGettingException(String message) {
		super(message);
	}
	
	public PersonGettingException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public PersonGettingException(Throwable cause) {
		super(cause);
	}
}
