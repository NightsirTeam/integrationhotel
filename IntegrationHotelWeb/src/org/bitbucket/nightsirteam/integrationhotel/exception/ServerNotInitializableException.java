package org.bitbucket.nightsirteam.integrationhotel.exception;

public class ServerNotInitializableException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ServerNotInitializableException() {
		super();
	}
	
	public ServerNotInitializableException(String message) {
		super(message);
	}
	
	public ServerNotInitializableException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public ServerNotInitializableException(Throwable cause) {
		super(cause);
	}
}
