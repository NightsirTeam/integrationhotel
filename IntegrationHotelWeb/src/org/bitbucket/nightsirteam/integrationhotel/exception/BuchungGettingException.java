package org.bitbucket.nightsirteam.integrationhotel.exception;

public class BuchungGettingException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BuchungGettingException() {
		super();
	}
	
	public BuchungGettingException(String message) {
		super(message);
	}
	
	public BuchungGettingException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public BuchungGettingException(Throwable cause) {
		super(cause);
	}
}
