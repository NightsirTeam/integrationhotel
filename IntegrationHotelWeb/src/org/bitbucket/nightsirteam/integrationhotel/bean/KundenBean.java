package org.bitbucket.nightsirteam.integrationhotel.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.bitbucket.nightsirteam.integrationhotel.model.NameFilter;
import org.bitbucket.nightsirteam.integrationhotel.model.Person;
import org.bitbucket.nightsirteam.integrationhotel.rmi.DBAccessor;

@ManagedBean
@ViewScoped
public class KundenBean {
	private NameFilter nameFilter;
	private List<Person> persons;
	
	public KundenBean() {
		nameFilter = new NameFilter();
		filterList();
	}
	
	public void filterList() {
		persons = DBAccessor.getInstance().getPersonsFiltered(nameFilter);
	}
	
	public List<Person> getPersons() {
		return persons;
	}
	
	public String getSurname() {
		return nameFilter.getSurname();
	}
	
	public void setSurname(String surname) {
		nameFilter.setSurname(surname);
	}
	
	public String getLastname() {
		return nameFilter.getLastname();
	}
	
	public void setLastname(String name) {
		nameFilter.setLastname(name);
	}
}
