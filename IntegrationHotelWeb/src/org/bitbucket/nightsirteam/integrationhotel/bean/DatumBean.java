package org.bitbucket.nightsirteam.integrationhotel.bean;

import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.bitbucket.nightsirteam.integrationhotel.model.PersonenBuchung;
import org.bitbucket.nightsirteam.integrationhotel.rmi.DBAccessor;

@ManagedBean
@ViewScoped
public class DatumBean {
	private Date date;
	
	private List<PersonenBuchung> personenBuchungen;
	
	public DatumBean() {
		date = new Date();
		filterList();
	}
	
	public void filterList() {
		personenBuchungen = DBAccessor.getInstance().getPersonByDate(date);
	}
	
	public List<PersonenBuchung> getPersonenBuchungen() {
		return personenBuchungen;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Date getDate() {
		return date;
	}
}
