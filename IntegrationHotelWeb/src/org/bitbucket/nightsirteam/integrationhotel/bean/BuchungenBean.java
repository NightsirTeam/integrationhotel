package org.bitbucket.nightsirteam.integrationhotel.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.bitbucket.nightsirteam.integrationhotel.model.Buchung;
import org.bitbucket.nightsirteam.integrationhotel.rmi.DBAccessor;

@ManagedBean
@RequestScoped
public class BuchungenBean {
	private List<Buchung> buchungen;
	
	public BuchungenBean() {}
	
	@PostConstruct
	public void init() {
		int kundenId = 1;
		try {
			kundenId = Integer.parseInt(FacesContext.getCurrentInstance().
					getExternalContext().getRequestParameterMap().get("kundenId"));
		} catch (NumberFormatException e) {}
		
		buchungen = DBAccessor.getInstance().getBuchungByPersId(kundenId);
	}
	
	public List<Buchung> getBuchungen() {
		return buchungen;
	}
}
